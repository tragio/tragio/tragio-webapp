#! /usr/bin/env sh

# Let the DB start
python app/scripts/entrypoint.py

# Run migrations
alembic upgrade head

# Create initial data in DB
python app/scripts/initial_data.py
