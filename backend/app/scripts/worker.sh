#! /usr/bin/env sh
set -e

sh app/scripts/wait_init_upgrade.sh

celery -A app.worker worker -l ${LOG_LEVEL:-info} -Q main-queue -c 1
