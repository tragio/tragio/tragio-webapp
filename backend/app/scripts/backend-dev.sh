#! /usr/bin/env sh

set -e

sh app/scripts/wait_init_upgrade.sh

sh -c "jupyter lab --ip=0.0.0.0 --allow-root --NotebookApp.custom_display_url=http://127.0.0.1:8888" &

python app/main.py
