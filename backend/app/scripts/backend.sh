#! /usr/bin/env sh
set -e

sh app/scripts/wait_init_upgrade.sh

gunicorn -k ${WORKER_CLASS:-uvicorn.workers.UvicornWorker} -c ${GUNICORN_CONF:-./app/conf/gunicorn_conf.py} ${APP_MODULE:-app.main:app}
