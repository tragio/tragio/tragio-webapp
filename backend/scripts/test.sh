#!/usr/bin/env sh

set -e
set -x

sh app/scripts/wait_init_upgrade.sh

pytest --cov=app --cov-report=term-missing tests "${@}"
